#!/usr/bin/env make -f

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MKFILE_PATH))))

LINES = `tput lines`
COLUMNS=`tput cols`

docker_exec = docker exec -e COLUMNS="$(COLUMNS)" -e LINES="$(LINES)" -e DOCKER_CUSTOM_ACTION='$(1)' -it $(CONTAINER_NAME) bash

CONTAINER_NAME = cms_ruby
START = rails s -b 0.0.0.0
CONSOLE = rails c
BUNDLE = bundle

start: start_container
	@echo "Starting application..."
	$(call docker_exec,$(START))

attach: start_container
	@echo "Attaching to container..."
	$(call docker_exec,)

console: start_container
	@echo "Starting rails console..."
	$(call docker_exec,$(CONSOLE))

stop:
	@echo "Stopping container..."
	@docker-compose stop

build:
	@echo "(Re)building image..."
	@docker-compose build

bundle: start_container
	@echo "Starting bundle..."
	$(call docker_exec,$(BUNDLE))

start_container:
	@echo "Starting container..."
	@docker-compose up -d
