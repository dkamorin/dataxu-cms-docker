#!/bin/bash

usage()
{
cat << EOF
Ivalid command

AVAILABLE OPTIONS:
   start      just starts container
   stop       stops container
   attach     starts container and attaches to application folder inside
EOF
}

case $1 in
  "")
    echo "Start"
    docker-compose up -d
    docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -it ${PWD##*/}_app_1 bash -c 'cd /usr/src/app; exec "${SHELL:-sh}"'
    ;;
  start)
    echo "Start"
    docker-compose up -d
    ;;
  stop)
    echo "Stop"
    docker-compose stop
    ;;
  attach)
    echo "Attach"
    docker-compose up -d
    docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -it ${PWD##*/}_app_1 bash -c 'cd /usr/src/app; exec "${SHELL:-sh}"'
    ;;
  build)
    echo "Build/rebuild"
    docker-compose build
    ;;
  *)
    echo "Error: Invalid command $1" >&2
    usage
    exit
    ;;
esac
